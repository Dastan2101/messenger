const express = require('express');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

router.get('/', [auth, permit('admin')], async (req, res) => {

    // const artists = await Artist.find();
    // const albums = await Album.find();
    // const tracks = await Track.find();
    //
    // const dataAll = {
    //     artists: artists,
    //     albums: albums,
    //     tracks: tracks
    // };
    //
    // res.send(dataAll);

});

router.post('/:id/publish', [auth, permit('admin')], async (req, res) => {
    const artist = await Artist.findById(req.params.id);
    const album = await Album.findById(req.params.id);
    const track = await Track.findById(req.params.id);


    if (artist) {
        artist.published = true;

        await artist.save();
    }
    if (album) {
        album.published = true;
        await album.save();
    }

    if (track) {
        track.published = true;
        await track.save();
    }

    const artists = await Artist.find();
    const albums = await Album.find();
    const tracks = await Track.find();

    const dataAll = {
        artists: artists,
        albums: albums,
        tracks: tracks
    };

    res.send(dataAll);

});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {

    // const artist = await Artist.findById(req.params.id);
    // const album = await Album.findById(req.params.id);
    // const track = await Track.findById(req.params.id);
    // if (artist) {
    //     artist.remove();
    //     await artist.save();
    // }
    // if (album) {
    //     album.remove();
    //     await album.save();
    // }
    //
    // if (track) {
    //     track.remove();
    //     await track.save();
    // }
    //
    // const artists = await Artist.find();
    // const albums = await Album.find();
    // const tracks = await Track.find();
    //
    // const dataAll = {
    //     artists: artists,
    //     albums: albums,
    //     tracks: tracks
    // };
    //
    // res.send(dataAll);

});

module.exports = router;