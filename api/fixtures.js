const nanoid = require('nanoid');

const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/Users');
const Messages = require('./models/Messages');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    await User.create({
            username: "user-1",
            password: "123",
            role: "user",
            token: nanoid()
        },
        {
            username: 'admin',
            password: '123',
            role: 'admin',
            token: nanoid()
        });

    await Messages.create(
        {
            text: 'First text',
            name: 'user-1'
        },
        {
            text: 'Third text',
            name: 'user-2'
        },
    );

    await connection.close();

};

run().catch(error => {
    console.error('Something went', error);
});