const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MessagesSchema = new Schema({
    text: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    }

});

const Messages = mongoose.model('Messages', MessagesSchema);

module.exports = Messages;