import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {connect} from "react-redux";
import Grid from "@material-ui/core/Grid/Grid";

const styles = {
    root: {
        flexGrow: 1,
    }

};

const URL = 'ws://localhost:8000/messenger?token=';


class MainPage extends Component {
    state = {
        messageText: '',
        onlineUsers: [],
        messages: []
    };

    ws = new WebSocket(URL)


    componentDidMount() {
        let token;
        if (this.props.user && this.props.user.token) {
            token = this.props.user.token
        }


        this.websocket = new WebSocket(URL + token);


        this.websocket.onmessage = event => {
            const decodedMessage = JSON.parse(event.data);

            if (decodedMessage.type === 'NEW_MESSAGE') {
                this.setState({
                    messages: [...this.state.messages, decodedMessage.messages]
                });
            } else if (decodedMessage.type === 'LATEST_MESSAGES') {
                this.setState({
                    messages: decodedMessage.messages.reverse()

                })

            }

            if (decodedMessage.type === 'ACTIVE_USERS') {
                this.setState({
                    onlineUsers: decodedMessage.username
                });

            }

            this.websocket.onclose = () => {
                // automatically try to reconnect on connection loss
                this.setState({
                    ws: new WebSocket(URL + token),
                })
            }

        };

    }

    componentWillUnmount() {
        this.websocket.close()
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    sendMessage = () => {
        if (!this.props.user) return null;
        const messages = JSON.stringify({
            type: 'CREATE_MESSAGE',
            text: this.state.messageText,
            username: this.props.user.username
        });

        this.websocket.send(messages);
        this.setState({messageText: ''})
    };


    render() {
        return (
            <div className={styles.root}>
                <Grid container spacing={24}>
                    <Grid item xs={5}>
                        <div>
                            <h3>Online users</h3>
                            {this.state.onlineUsers.map((user, i) => (
                                <ul key={i}>
                                    <li>{user}</li>
                                </ul>
                            ))}
                        </div>
                    </Grid>
                    <Grid item xs={7}>
                        <h3>Chat</h3>
                        <div>
                            {this.state.messages.map((message, i) => (
                                <div key={i}>
                                    <b>{message.name}</b>:
                                    <span>{message.text}</span>
                                </div>
                            ))}
                            <div>
                                <input
                                    type="text"
                                    name="messageText"
                                    value={this.state.messageText}
                                    onChange={this.inputChangeHandler}
                                />
                                <input type="button" value="Send" onClick={this.sendMessage}/>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});


export default connect(mapStateToProps)(withStyles(styles)(MainPage));