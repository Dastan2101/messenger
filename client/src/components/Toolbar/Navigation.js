import React, {Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom'
import IconButton from '@material-ui/core/IconButton';

const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    title: {
        marginRight: 30
    }
};
const Navigation = ({user, logoutUser}) => {

    return (
        <div style={styles.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" color="inherit" style={styles.grow}>
                        <Button color="inherit" component={Link} to="/">
                            WebSocket
                        </Button>
                    </Typography>
                    {!user ? (
                        <Fragment>
                            <Button color="inherit" component={Link} to="/registration">Sign Up</Button>
                            <Button color="inherit" component={Link} to="/login">Login</Button>
                        </Fragment>
                    ) : <Fragment>
                        <h4 style={styles.title}>
                            {"Hello, " + user.username.toUpperCase()}
                        </h4>
                        {user && user.role === 'admin' ? <IconButton color="inherit" component={Link} to="/admin">

                        </IconButton> : null}
                        <Button color="inherit" onClick={logoutUser}>Logout</Button>
                    </Fragment>
                    }
                </Toolbar>
            </AppBar>
        </div>
    );
};

export default withStyles(styles)(Navigation);