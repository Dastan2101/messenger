import React from 'react';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit
    }
});

const InputItem = props => {
    return (
        <TextField
            label={props.label}
            type={props.type}
            value={props.value}
            name={props.name}
            onChange={props.onChange}
            className={styles.textField}
            required={props.required}
            margin="dense"
            variant="outlined"
        />

    );
};

export default withStyles(styles)(InputItem);