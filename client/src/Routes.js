import React from 'react';
import MainPage from "./container/MainPage/MainPage";
import Registr from "./container/Registr/Registr";
import Login from "./container/Login/Login";

import {Redirect, Route, Switch} from "react-router-dom";

const ProtectedRoute = ({isAllowed, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={MainPage}/>
            <ProtectedRoute
                isAllowed={user && user.role === 'admin'}
                path="/admin/"
                exact
                component={MainPage}
            />
            <Route path="/registration" exact component={Registr}/>
            <Route path="/login" exact component={Login}/>
        </Switch>
    );
};

export default Routes;